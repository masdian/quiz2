<!DOCTYPE html>
<html>
<body>

<?php

function hitung($string_data) {
    if (strpos($string_data, "*") == true) {
        $x = substr($string_data, 0, strpos($string_data, "*"));
        $y = substr($string_data, strpos($string_data, "*") + 1, strlen($string_data) - strpos($string_data, "*") - 1);
        echo (int)$x * (int)$y;
    } else if (strpos($string_data, "+") == true) {
        $x = substr($string_data, 0, strpos($string_data, "+"));
        $y = substr($string_data, strpos($string_data, "+") + 1, strlen($string_data) - strpos($string_data, "+") - 1);
        echo (int)$x + (int)$y;
    } else if (strpos($string_data, ":") == true) {
        $x = substr($string_data, 0, strpos($string_data, ":"));
        $y = substr($string_data, strpos($string_data, ":") + 1, strlen($string_data) - strpos($string_data, ":") - 1);
        echo (int)$x / (int)$y;
    } else if (strpos($string_data, "%") == true) {
        $x = substr($string_data, 0, strpos($string_data, "%"));
        $y = substr($string_data, strpos($string_data, "%") + 1, strlen($string_data) - strpos($string_data, "%") - 1);
        echo (int)$x % (int)$y;
    } else if (strpos($string_data, "-") == true) {
        $x = substr($string_data, 0, strpos($string_data, "-"));
        $y = substr($string_data, strpos($string_data, "-") + 1, strlen($string_data) - strpos($string_data, "-") - 1);
        echo (int)$x - (int)$y;
    } else {
        echo "simbol operasi hitung tidak ditemukan atau tidak terdaftar";
    }
}

echo hitung("102*2");
echo "<br>";
echo hitung("2+3");
echo "<br>";
echo hitung("100:25");
echo "<br>";
echo hitung("10%2");
echo "<br>";
echo hitung("99-2");
echo "<br>";
echo hitung("100/5");

?>
 
</body>
</html>