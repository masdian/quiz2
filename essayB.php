<!DOCTYPE html>
<html>
<body>

<?php

/*
Yth. Trainer Laravel. Ketika saya submit kuis, saya hanya sempat membuat fungsi essay B seperti berikut

function perolehan_medali($string) {
    if ($string == []) {
        echo "no data";
    }
}

Selanjutnya saya mengerjakan sampai selesai karena penasaran dan diperoleh hasil berikut yang saya rasa
sudah sesuai atau mirip dengan permintaan. Mohon tidak dimasukkan ke dalam penilaian ya karena saya saat 
ujian baru sempet buat 3 baris fungsi :-)
yang lengkap ini saya save di gitlab hanya untuk menghilangkan penasaran saja

Terima kasih dan salam.

*/

function perolehan_medali($string) {
    if ($string == []) {
        echo "no data";
    } else {
        $a = [];
        for ($i = 0; $i < sizeof($string); $i++) {
            array_push($a, join(" ", $string[$i]));
        }
        $negara = [];
        for ($i = 0; $i < sizeof($string); $i++) {
            array_push($negara, $string[$i][0]);
        }
        $b = array_unique($negara);
        $hasil = [];
        for ($j = 0; $j < sizeof($b); $j++) {
            array_push($hasil, ["negara" => $b[$j],
                                "emas" => count(array_keys($a, $b[$j] . " " . "emas")),
                                "perak" => count(array_keys($a, $b[$j] . " " . "perak")),
                                "perunggu" => count(array_keys($a, $b[$j] . " " . "perunggu"))]);
        }
        print_r($hasil);
    }

}

$data = array(
    array('Indonesia', 'emas'),
    array('India', 'perak'),
    array('Korea Selatan', 'emas'),
    array('India', 'perak'),
    array('India', 'emas'),
    array('Indonesia', 'perak'),
    array('Indonesia', 'emas')
);

echo "ini output menggunakan array contoh";
echo "<br><br>";
perolehan_medali($data);
echo "<br><br>";
echo "ini output jika input adalah array kosong";
echo "<br><br>";
perolehan_medali([]);



?>
 
</body>
</html>